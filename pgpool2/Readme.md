### 重点说明  
此为 demo repmgr(replica manager) + pgpool   

      REPMGR_PRIMARY_HOST=pg-0 --->此为预设的 replica primary host

如果有变动(primary crash)切换成其他台的时后，其他台在启动时，要修改为正确启动路径  
可以进入每台看 replication status   
     
     sudo docker-compose exec pg-0(serverName) bash   (进入指令)
    
执行
    
    /opt/bitnami/scripts/postgresql-repmgr/entrypoint.sh repmgr -f /opt/bitnami/repmgr/conf/repmgr.conf  cluster show

确认那台为Master 主机

查看 repmgr event
    
    /opt/bitnami/scripts/postgresql-repmgr/entrypoint.sh repmgr -f /opt/bitnami/repmgr/conf/repmgr.conf  cluster event
    
注意如果当机器 shutdown时后，会让 repmgr 服务一起挂点，所以必须要重启服务，必须调整  

      REPMGR_PRIMARY_HOST=pg-0 

记得重启之后，要看整体同步的状态

    PGPASSWORD=$PGPOOL_POSTGRES_PASSWORD psql -U $PGPOOL_POSTGRES_USERNAME -h localhost


测试一:  
主库脱离之后，从库会被重新选择为主，但是原来的主库，会变成failed的状态。  

     ID   | Name | Role    | Status    | Upstream | Location | Priority | Timeline | Connection string                                                                      
	 ------+------+---------+-----------+----------+----------+----------+----------+-----------------------------------------------------------------------------------------
     1000 | pg-0 | primary | - failed  | ?        | default  | 100      |          | user=repmgr password=repmgrpassword host=pg-0 dbname=repmgr port=5432 connect_timeout=5  
     1001 | pg-1 | primary | * running |          | default  | 100      | 2        | user=repmgr password=repmgrpassword host=pg-1 dbname=repmgr port=5432 connect_timeout=5
     1002 | pg-2 | standby |   running | pg-1     | default  | 100      | 2        | user=repmgr password=repmgrpassword host=pg-2 dbname=repmgr port=5432 connect_timeout=5

执行脱离不可用的主库(node Id 就是上图ID)。

     /opt/bitnami/scripts/postgresql-repmgr/entrypoint.sh repmgr primary unregister -f /opt/bitnami/repmgr/conf/repmgr.conf --node-id=1000

重新启动之后，会重新加入变成 standby 状态。

由于没有开发 witness 的 role ，所以此(bitnami-docker-postgresql-repmgr)切换时会有问题，无法自动切换。需要跟上面一样，做手动切换。

