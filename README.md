# Docker-Compose  
----  
## Docker-Compose Examples
1. include MongoDB singleton instance initialize.
2. include Postgres singleton instance initialize.
3. include RabbitMQ singleton instance initialize.
4. include Redis singleton instance initialize.
5. include Tomcat singleton instance initialize.
6. include Gitlab instance initialize.
&nbsp; 
&nbsp;

# 新增 Docker 簡易指令操作；
*** 
## 回收無用的空間：
    sudo docker system prune
***  
## 更新 docker image： 
    sudo docker pull
***  
## 移除舊的 cotainer：
    sudo  docker-compose down
***
## 連線進入 cotainer：    
    sudo docker exec -it <container_id> bash
***
## 檢查 cotainerId：    
    sudo docker ps
***
## 檢查 docker cpu status :
	sudo docker stats

# 新增纯真资料库使用
----
纯真(CZ88.NET)自2005年起一直为广大社区用户提供社区版IP地址库，只要获得纯真的授权就能免费使用，并不断获取后续更新的版本。如果有需要免费版IP库的朋友可以前往纯真的官网进行申请。

纯真除了免费的社区版IP库外，还提供数据更加准确、服务更加周全的商业版IP地址查询数据。纯真围绕IP地址，基于 网络空间拓扑测绘 + 移动位置大数据 方案，对IP地址定位、IP网络风险、IP使用场景、IP网络类型、秒拨侦测、VPN侦测、代理侦测、爬虫侦测、真人度等均有近20年丰富的数据沉淀。
